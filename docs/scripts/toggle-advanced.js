// Restrain the scope
(function() {

    const advancedBoxs = $('.advancedBox');
    const advancedSwitch = $('.switch-input');

    if (advancedBoxs.length === 0 || advancedSwitch.length === 0)
        return;

    /**
     * This function will control url parameters
     */
    function handleUrlMode () {

        let searchQueries = window.location.search;
        
        if (searchQueries === '?mode=advanced')
            window.history.replaceState({}, document.title, window.location.pathname);
        else
            window.history.pushState({}, document.title, window.location.pathname + "?mode=advanced");

        return searchQueries
    }

    /**
     * This function will control advanced boxes visibility
     */
    function handleVisibility (mode) {
        if (mode == '?mode=advanced') {
            advancedBoxs.show();
            advancedSwitch.attr('checked', "");
        } else {
            advancedBoxs.hide();
            advancedSwitch.removeAttr('checked');
        }
    }

    /**
     * This function initialise the module
     */
    function init () {

        handleVisibility(window.location.search);

        advancedSwitch.on('click', ev => {
            handleUrlMode();
            handleVisibility(window.location.search);
        })
    }
    jQuery(document).ready(init);

})();