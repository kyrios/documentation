# Introduction

Cette partie concerne les utilisateurs qui veulent contribuer à Solus en fournissant rapports de bogues, demandes de paquets, demande de mise à jour de paquets. Afin de réaliser ces actions il vous faudra avoir un compte (pensez à vous inscrire si cela n'est pas fait) sur la plateforme [dev.getsol.us](https//dev.getsol.us/) qui utilise [Phabricator](https://fr.wikipedia.org/wiki/Phabricator/).
