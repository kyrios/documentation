# Introduction

Cette documentation vous permettra d'apprendre à *packager/empaqueter* des logiciels pour **Solus**. Le *packaging* est ce qui permet à une distribution de proposer des logiciels prets à l'installation, cela revient en fait à construire le paquet depuis les sources du logiciel.

!!! Note
    Dans ce manuel, le "repository" (dossier dans lequel les paquets vont être constuits) se nomme **Solbuild** et se trouve dans notre dossier personnel.
    Si vous décidez de le nommer autrement ou de le créer à un autre endroit, n'oubliez pas d'adapter les chemins indiqués dans cette documentation (par exemple `$HOME/Solbuild` ou `~/Solbuild`) pour le faire correspondre à celui qui vous avez crée sur votre machine.
